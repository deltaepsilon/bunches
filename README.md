# bunches

## Installation

- Install Docker and Docker Compose
- Install [Docker Windows Volume Watcher](https://github.com/merofeev/docker-windows-volume-watcher)
- If on Windows, run `yarn windows:watch` to get around the [Windows 10 iNotify bug](https://github.com/moby/moby/issues/30105)
- Run `yarn dev` to build the containers and shell into the `dev` container.
- Once in the `dev` container, run `yarn start` to bring the app up on localhost.

## Testing

To test within the container:

- Run `yarn dev` and then `yarn test` from inside the container.

To test from outside the container:

- Run `yarn build` to build.
- Run `yarn test` to test.

## Testing functions

Functions need to be tested from inside the `dev` container.


- Run `yarn dev` to enter the `dev` container.
- Run `cd functions && yarn test` to test the functions.

## Deploying

Simply pushing to the GitLab repo will trigger a CI build and deploy. See `.gitlab-ci.yml`.

Alternatively, deploy from within the `dev` container.

- Run `yarn dev` to shell into the `dev` container.
- From inside the `dev` container run `yarn deploy`, `yarn deploy:functions` or `yarn deploy:hosting`.

## Debugging

Nasty testing bugs can be debugged within VSCode.

I've checked in the `.vscode` folder to make this easier. 

- Close out of the `dev` container if it's open.
- Run `docker-compose down` to make sure all instances are cleaned up.
- Go to VSCodes's Debug tab and run the `Tests in Docker` job.
- Select **Debug Anyway** if you get an error prompt.
- Set breakpoints and let the first auto-breakpoint play through!


## Gitlab

### Make sure to base64 encode your service-acount.json file

See [Export secret file to Gitlab pipeline](https://medium.com/@michalkalita/export-secret-file-to-gitlab-pipeline-75789eee35bd)

### Set the environment variables in GitLab

- FIREBASE_DATABASE_URL
- FIREBASE_TOKEN, obtained by running `yarn ci:login` from inside the `dev` container.
- GIPHY_API_KEY
- SERVICE_ACCOUNT_BASE64
