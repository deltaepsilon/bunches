module.exports = function getDefaultState({ deck, dealt, matches, strikes, selectedIndexes } = {}) {
  return {
    deck: deck || [],
    dealt: dealt || [],
    matches: matches || [],
    strikes: strikes || [],
    selectedIndexes: new Set(selectedIndexes || []),
  };
};
