const getInitialState = require('../get-initial-state');
const constants = require('../../constants');

describe('#getInitialState', () => {
  describe('normal mode', () => {
    let state;

    beforeAll(() => {
      state = getInitialState();
    });

    it('should have 81 cards in the deck', () => {
      expect(state.deck.length).toEqual(81);
    });

    it('should have empty dealt param', () => {
      expect(state.dealt.length).toEqual(0);
    });

    it('should have empty matches param', () => {
      expect(state.matches.length).toEqual(0);
    });

    it('should have empty strikes param', () => {
      expect(state.strikes.length).toEqual(0);
    });

    it('should have an empty selectedIndexes param', () => {
      expect(state.selectedIndexes.size).toEqual(0);
    });
  });

  describe('easy mode', () => {
    let state;

    beforeAll(() => {
      state = getInitialState({ mode: constants.GAME_MODES.EASY });
    });

    it('should have 27 cards in the deck', () => {
      expect(state.deck.length).toEqual(27);
    });

    it('should have empty dealt param', () => {
      expect(state.dealt.length).toEqual(0);
    });

    it('should have empty matches param', () => {
      expect(state.matches.length).toEqual(0);
    });

    it('should have empty strikes param', () => {
      expect(state.strikes.length).toEqual(0);
    });

    it('should have an empty selectedIndexes param', () => {
      expect(state.selectedIndexes.size).toEqual(0);
    });
  });
});
