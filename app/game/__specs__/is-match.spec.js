const isMatch = require('../is-match');

describe('#isMatch', () => {
  const sameMatch = [
    { d1: 1, d2: 1, d3: 1, d4: 1 },
    { d1: 1, d2: 1, d3: 1, d4: 1 },
    { d1: 1, d2: 1, d3: 1, d4: 1 },
  ];
  const differentMatch = [
    { d1: 1, d2: 1, d3: 1, d4: 1 },
    { d1: 2, d2: 1, d3: 1, d4: 1 },
    { d1: 3, d2: 1, d3: 1, d4: 1 },
  ];
  const notMatch = [
    { d1: 1, d2: 1, d3: 1, d4: 1 },
    { d1: 2, d2: 1, d3: 1, d4: 1 },
    { d1: 1, d2: 1, d3: 1, d4: 1 },
  ];

  it('should throw if 0 cards', () => {
    expect(() => isMatch(Array(0))).toThrowErrorMatchingSnapshot();
  });

  it('should throw if 4 cards', () => {
    expect(() => isMatch(Array(4))).toThrowErrorMatchingSnapshot();
  });

  it('should accept a "same" match', () => {
    expect(isMatch(sameMatch)).toEqual(true);
  });

  it('should accept a "different" match', () => {
    expect(isMatch(differentMatch)).toEqual(true);
  });

  it('should reject a "not" match', () => {
    expect(isMatch(notMatch)).toEqual(false);
  });
});
