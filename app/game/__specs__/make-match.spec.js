const getInitialState = require('../get-initial-state');
const findMatches = require('../find-matches');
const dealCards = require('../deal-cards');
const makeMatch = require('../make-match');

describe('#makeMatch', () => {
  let initialState;
  let firstCubeState;
  let matches;
  let unmatchedCards;

  beforeEach(() => {
    initialState = getInitialState();
    firstCubeState = dealCards(initialState, 12);
    matches = findMatches(firstCubeState.dealt);
    unmatchedCards = firstCubeState.dealt.slice(0, 3);
  });

  it('should throw if cards are not a match', () => {
    expect(() => makeMatch(firstCubeState, unmatchedCards)).toThrowErrorMatchingSnapshot();
  });

  it('should make a match', () => {
    const state = makeMatch(firstCubeState, matches[0]);

    expect(state.matches.length).toEqual(1);
    expect(state.dealt.length).toEqual(9);
  });
});
