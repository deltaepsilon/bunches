const dealCards = require('../deal-cards');
const getInitialState = require('../get-initial-state');

describe('#dealCards', () => {
  const initialState = getInitialState();

  it('should throw if too many are requested', () => {
    expect(() => dealCards(initialState, 82)).toThrowErrorMatchingSnapshot();
  });

  it('should deal three cards by default', () => {
    const state = dealCards(initialState);

    expect(state.deck.length).toEqual(78);
    expect(state.dealt.length).toEqual(3);
  });

  it('should deal 12 cards', () => {
    const state = dealCards(initialState, 12);

    expect(state.deck.length).toEqual(69);
    expect(state.dealt.length).toEqual(12);
  });

  it('should deal 12 then 3 cards without side effects', () => {
    const state1 = dealCards(initialState, 12);
    const state2 = dealCards(state1);

    expect(initialState.deck.length).toEqual(81);
    expect(state1.deck.length).toEqual(69);
    expect(state1.dealt.length).toEqual(12);
    expect(state2.deck.length).toEqual(66);
    expect(state2.dealt.length).toEqual(15);
  });
});
