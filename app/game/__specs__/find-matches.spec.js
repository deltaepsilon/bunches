const getAllCardsSorted = require('../../utilities/get-all-cards-sorted');
const findMatches = require('../find-matches');

describe('#isMatch', () => {
  const allCardsSorted = getAllCardsSorted();
  const row1 = allCardsSorted.filter(({d1}) => d1 == 1)
  const cube1 = row1.filter(({d2}) => d2 == 1)


  it('should find 12 matches in cube1', () => {
    const matches = findMatches(cube1);

    expect(matches.length).toEqual(12);
  });

  it('should find 117 matches in row1', () => {
    const matches = findMatches(row1);

    expect(matches.length).toEqual(117);
  });

  it('should find 1080 matches in allCardsSorted', () => {
    const matches = findMatches(allCardsSorted);

    expect(matches.length).toEqual(1080);
  });
});
