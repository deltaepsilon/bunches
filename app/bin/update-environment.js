const fs = require('fs');
const replace = require('replace-in-file');

(async function updateEnvironment([...files], [...variables]) {
  const filesToCopy = files.map(file => ({ dest: file, src: file + '.dist' }));
  const options = {
    files,
    from: variables.map(variable => new RegExp(variable, 'g')),
    to: variables.map(variable => process.env[variable]),
  };

  filesToCopy.forEach(({ src, dest }) => fs.copyFileSync(src, dest));

  const changes = await replace(options);

  console.info('changes', changes);
})(['./functions/environment.json'], ['FIREBASE_DATABASE_URL', 'GIPHY_API_KEY']);
