import getStrikesByUid from './get-strikes-by-uid';

export default function getStrikesByUidFlat(game) {
  const strikesByUid = getStrikesByUid(game);

  return Object.keys(strikesByUid).map(uid => ({ uid, strikes: strikesByUid[uid] }));
}
