module.exports = function findCardIndex(cards, cardToMatch) {
  return cards.findIndex(
    card =>
      card.d1 == cardToMatch.d1 &&
      card.d2 == cardToMatch.d2 &&
      card.d3 == cardToMatch.d3 &&
      card.d4 == cardToMatch.d4
  );
};
