import getRandomName from '../../utilities/get-random-name';

export default () => ({ randomName: getRandomName() });
