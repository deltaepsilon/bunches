/* globals window */
import constants from '../constants';
import getInitialState from '../game/get-initial-state';
const rootRef = window.firebase.database().ref('games');

export default ({ uid, email }, { type, gameMode }) => {
  const mode = gameMode;
  const game = getInitialState({ mode });

  const value = {
    uid,
    email,
    type,
    game: { ...game, state: constants.GAME_STATES.PENDING },
    mode,
    startingDeck: game.deck,
    created: Date.now(),
  };
  const ref = rootRef.push(value);

  return { key: ref.key, value };
};
