/* globals window */
const rootRef = window.firebase.database().ref('games');

export default (key) => {
  const ref = rootRef.child(key);

  return ref.remove();
};
