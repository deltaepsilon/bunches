const functions = require('firebase-functions');
const context = require('./utilities/prod-context');
const { GetGif } = require('./src');

exports.getGif = functions.https.onCall(GetGif(context));
