const admin = require('firebase-admin');
const environment = require('../environment.json');

admin.initializeApp();

module.exports = { admin, environment };
