const axios = require('axios');

module.exports = ({ environment }) => async ({ tag }, { auth } = {}) => {
  const { apiKey: api_key } = environment.giphy;

  const { data } = await axios.get('https://api.giphy.com/v1/gifs/random', {
    params: { tag, api_key },
  });

  return data;
};
