const context = require('../../utilities/test-context');
const getGif = require('../get-gif')(context);

describe('#getGif', () => {
  it('should get a random gif with a tag', async () => {
    const result = await getGif({ tag: 'celebration' });

    expect(result.data.type).toEqual('gif');
  });
});
